using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterMovement : MonoBehaviour
{
    NavMeshAgent agent;
    public Transform target;

    public float movementSpeed;

    // Start is called before the first frame update
    void Start()
    {
        // StartCoroutine("repathToTarget");
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 targetPos = new Vector3(target.position.x, target.position.y + 1, target.position.z);
        transform.position = Vector3.MoveTowards(transform.position, targetPos, movementSpeed);
    }

    IEnumerator repathToTarget()
    {
        yield return new WaitForSeconds(3);
        //    agent.SetDestination(target.position);
        //  StartCoroutine("repathToTarget");
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            //other.GetComponent<ClickToMove>().FleeToRandomPoint();
            other.GetComponent<PlayerStats>().TakeDamage(0.2f);
        }
    }
}
