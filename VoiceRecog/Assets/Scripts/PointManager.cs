using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointManager : MonoBehaviour
{
    public Transform[] interestPoints;
    public Transform player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CheckActivePoints(string result)
    {
        foreach (Transform point in interestPoints)
        {
            print(point.name);
            //if a point name has a name matching the command, order the player to that point
            if (result == point.name)
            {
                //print("voice match to point detected");
             //   player.GetComponent<ClickToMove>().MoveToInterestPoint(point);
                point.GetComponent<InterestPoint>().ActivatePoint();
            }
        }
    }


}
