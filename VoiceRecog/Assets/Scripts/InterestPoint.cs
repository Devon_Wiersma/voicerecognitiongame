using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterestPoint : MonoBehaviour
{

    BoxCollider collider;
    public Transform lookPoint;

    public void Start()
    {
        collider = GetComponent<BoxCollider>();
    }

    public void ActivatePoint()
    {
        collider.enabled = true;
    }

    public void DeactivatePoint()
    {
        collider.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        print("player in trigger");
        DeactivatePoint();
        if (other.GetComponent<LocomotionSimpleAgent>())
        {
            other.GetComponent<LocomotionSimpleAgent>().PlayInteractionAnimation(lookPoint);
        }

    }


}
