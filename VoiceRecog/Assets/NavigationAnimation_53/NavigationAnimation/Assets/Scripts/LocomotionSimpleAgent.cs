﻿using UnityEngine;
using UnityEngine.AI;

//[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
//[RequireComponent(typeof(Animator))]
public class LocomotionSimpleAgent : MonoBehaviour
{
    public Animator anim;
    public UnityEngine.AI.NavMeshAgent agent;
    Vector2 smoothDeltaPosition = Vector2.zero;
    Vector2 velocity = Vector2.zero;

    Vector3 previousPosition;

    Transform targetPoint;

    public bool isRunning;

    public float defaultAgentSpeed;


    void Start()
    {
        //agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.updatePosition = false;
        defaultAgentSpeed = agent.speed;

    }

    void Update()
    {
        // agent.SetDestination(agent.destination);
        Vector3 worldDeltaPosition = agent.nextPosition - transform.position;

        // Map 'worldDeltaPosition' to local space
        float dx = Vector3.Dot(transform.right, worldDeltaPosition);
        float dy = Vector3.Dot(transform.forward, worldDeltaPosition);
        Vector2 deltaPosition = new Vector2(dx, dy);

        // Low-pass filter the deltaMove
        float smooth = Mathf.Min(1.0f, Time.deltaTime / 0.15f);
        smoothDeltaPosition = Vector2.Lerp(smoothDeltaPosition, deltaPosition, smooth);

        // Update velocity if delta time is safe
        if (Time.deltaTime > 1e-5f)
            velocity = smoothDeltaPosition / Time.deltaTime;

        bool shouldMove = velocity.magnitude > 0.01f && agent.remainingDistance > 0.1f;

        // Update animation parameters
        anim.SetBool("move", shouldMove);
        anim.SetFloat("velx", velocity.x);
        anim.SetFloat("vely", velocity.y);

        //compare previous position with current one to get speed
        Vector3 curMove = transform.position - previousPosition;
        float curSpeed = (transform.position - previousPosition).magnitude / Time.deltaTime;
        previousPosition = transform.position;

        if ((curSpeed > 1) && (agent.speed == defaultAgentSpeed * 2))
        {
            isRunning = true;
        }
        else
        {
            isRunning = false;
        }

        anim.SetFloat("MovementSpeed", curSpeed);

        LookAt lookAt = GetComponent<LookAt>();
        if (lookAt)
            lookAt.lookAtTargetPosition = agent.steeringTarget + transform.forward;

    }

    public void RevertToIdle()
    {
        agent.isStopped = false;
        anim.SetBool("isInteracting", false);
    }

    void OnAnimatorMove()
    {
        // Update postion to agent position
        transform.position = agent.nextPosition;

        //// Update position based on animation movement using navigation surface height
        //Vector3 position = anim.rootPosition;
        //position.y = agent.nextPosition.y;
        //transform.position = position;

    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.tag == "Interactible")
        //{
        //    agent.isStopped = true;
        //    anim.SetBool("isInteracting", true);
        //    other.enabled = false;
        //    //RevertToIdle();
        //}
    }

    public void PlayInteractionAnimation(Transform turnPoint)
    {
        agent.isStopped = true;
        anim.SetBool("isInteracting", true);

        print("playing interaction animation");


        Vector3 lookVector = transform.position - turnPoint.position;

    }

    public void ListenForMovementCommands(string result)
    {
        //if a point name has a name matching the command, order the player to that point
        if (result == "run")
        {
            agent.speed = defaultAgentSpeed * 2;
            print("Player switch to run");
        }

        if (result == "walk")
        {
            isRunning = false;
            agent.speed = defaultAgentSpeed;
            print("Player switch to walk");
        }
    }

    public void ListenForDirectionCommands(string result)
    {
        if (GameObject.Find(result) != null)
        {
            Transform targetPoint = GameObject.Find(result).transform;
            if (targetPoint.gameObject.activeSelf == true)
            {
                agent.isStopped = true;
                NavMeshPath path = new NavMeshPath();
                agent.SetDestination(targetPoint.position);
                agent.isStopped = false;
            }
        }

        //if a point name has a name matching the command, order the player to that point
        if (result == "north")
        {
            agent.SetDestination(GameObject.Find(result).transform.position);
            print("Player switch to run");
        }

        if (result == "west")
        {
            agent.SetDestination(GameObject.Find("West").transform.position);
            agent.speed = defaultAgentSpeed;
            print("Player switch to walk");
        }

        targetPoint = null;
    }

    public void MoveToInterestPoint(Transform dest)
    {
        if (!anim.GetBool("isInteracting"))
        {

        }
    }

}
