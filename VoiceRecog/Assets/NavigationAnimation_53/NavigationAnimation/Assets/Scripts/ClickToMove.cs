﻿// ClickToMove.cs
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class ClickToMove : MonoBehaviour
{
    RaycastHit hitInfo = new RaycastHit();
    public UnityEngine.AI.NavMeshAgent agent;

    public bool isFleeing = false;
    Transform fleePoint;

    LocomotionSimpleAgent locomotionScript;

    public Camera camera;

    public string upCmd = "up";
    public string downCmd = "down";
    public string leftCmd = "left";
    public string rightCmd = "right";

    void Start()
    {
        //    agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        locomotionScript = GetComponent<LocomotionSimpleAgent>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
                agent.destination = hitInfo.point;

        }

        if (agent.remainingDistance < 1)
        {
            isFleeing = false;
        }
    }

    public void RecognitionEvent(string recognition)
    {
        if (recognition == upCmd)
        {
            agent.destination = GameObject.Find("UpCube").transform.position;
        }
        if (recognition == downCmd)
        {
            agent.destination = GameObject.Find("DownCube").transform.position;
        }
        if (recognition == leftCmd)
        {
            agent.destination = GameObject.Find("LeftCube").transform.position;
        }
        if (recognition == rightCmd)
        {
            agent.destination = GameObject.Find("RightCube").transform.position;
        }


    }

    //public void MoveToInterestPoint(Transform dest)
    //{
    //    if (!locomotionScript.anim.GetBool("isInteracting"))
    //    {
    //        agent.isStopped = true;
    //        NavMeshPath path = new NavMeshPath();
    //        agent.SetDestination(dest.position);
    //        agent.isStopped = false;
    //    }
    //}
}
