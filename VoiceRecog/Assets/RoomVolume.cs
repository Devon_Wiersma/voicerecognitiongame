﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomVolume : MonoBehaviour
{
    public GameObject[] gameObjectsToToggleOnPlayerLeave;
    public Camera cameraToEnable;

    // Start is called before the first frame update
    void Start()
    {
        //foreach (GameObject toggledObjects in gameObjectsToToggleOnPlayerLeave)
        //{
        //    toggledObjects.SetActive(false);
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            print("Enter Room");
            // cameraToEnable.enabled = true;
            //foreach (GameObject toggledObjects in gameObjectsToToggleOnPlayerLeave)
            //{
            //    toggledObjects.SetActive(true);
            //}

            // cameraToEnable.enabled = true;
            GameObject.FindGameObjectWithTag("Player").GetComponent<ClickToMove>().camera = cameraToEnable;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            print("Exit Room");
            //  cameraToEnable.enabled = false;

            //foreach (GameObject toggledObjects in gameObjectsToToggleOnPlayerLeave)
            //{
            //    toggledObjects.SetActive(false);
            //}

            //cameraToEnable.enabled = false;
        }
    }
}
