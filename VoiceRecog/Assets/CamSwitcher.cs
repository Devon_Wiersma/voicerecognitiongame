﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitcher : MonoBehaviour
{
    public Camera[] allCameras;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SwitchToCamera(int cameraNum)
    {
        DisableAllCameras();

        allCameras[cameraNum].enabled = true;
    }

    public void DisableAllCameras()
    {
        foreach(Camera cam in allCameras)
        {
            cam.enabled = false;
        }
    }
}
