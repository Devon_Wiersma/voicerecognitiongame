﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public float playerHealth = 100;
    public float playerStamina = 100;

    public LocomotionSimpleAgent playerLocomotion;

    // Start is called before the first frame update
    void Start()
    {
        playerLocomotion = GetComponent<LocomotionSimpleAgent>();
    }

    // Update is called once per frame
    void Update()
    {

        playerStamina = Mathf.Clamp(playerStamina, 0, 100);

        if (playerLocomotion.isRunning)
        {
            playerStamina = playerStamina - 0.1f;
        }
        else if (playerStamina < 100)
        {
            playerStamina = playerStamina + 0.1f;
        }
    }

    public void TakeDamage(float damageValue)
    {
        playerHealth = Mathf.Clamp(playerHealth, 0, 100);
        playerHealth = playerHealth - damageValue;
    }
}
